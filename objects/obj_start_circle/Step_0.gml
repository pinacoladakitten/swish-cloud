if active = 0
{
if mouse_check_button(mb_left)
{
image_xscale = min(image_xscale+3,3)
}

if !mouse_check_button(mb_left)
{
image_xscale = max(image_xscale-3,0)
}
}
image_yscale = image_xscale
if image_xscale = 3
{
instance_create(x,y,obj_hit)
active = 1
    with obj_cloudly
    {
    game_start02 = 1
    alarm[0] = 5*room_speed
    }
    
    with obj_obs_spawner
    {
    alarm[0] = 1*room_speed
    }
}

if active = 1
{
image_xscale = max(image_xscale-0.1,0)
image_yscale = image_xscale
}

if image_xscale = 0
{
instance_destroy();
}

if instance_exists(obj_cloudly)
{
x = obj_cloudly.x
y = obj_cloudly.y
}

with obj_spikes
{
instance_destroy();
}

