if get_out = 0
{
if obj_shop_menu.page = 0
{
    if char_select = 2
    if points_total >= 150
    {
    points_total -= 150
    global.char_bought[1] = 1
    audio_play_sound(snd_buy,10,false)
    }
    if char_select = 3
    if points_total >= 1000
    {
    points_total -= 1000
    global.char_bought[2] = 1
    audio_play_sound(snd_buy,10,false)
    with obj_cloudly
        {
        hp = 2
        }
    }
    if char_select = 4
    if points_total >= 800
    {
    points_total -= 800
    global.char_bought[3] = 1
    audio_play_sound(snd_buy,10,false)
    }
    if char_select = 5
    if points_total >= 950
    {
    points_total -= 950
    global.char_bought[4] = 1
    audio_play_sound(snd_buy,10,false)
    }
    if char_select = 6
    if points_total >= 550
    {
    points_total -= 550
    global.char_bought[5] = 1
    audio_play_sound(snd_buy,10,false)
    }
}
if obj_shop_menu.page = 1
{
    if obj_lightswitch_shop.light_select = 1
    {
    points_total -= 80
    lightswitch_bought = 1
    audio_play_sound(snd_buy,10,false)
    }
}
char = char_select
ini_open("savedata.ini")
ini_write_real("save01","PointsTotal",points_total)
ini_write_real("save01","MiscBought01",lightswitch_bought)
ini_write_real("save01","CharBought01",global.char_bought[1])
ini_write_real("save01","CharBought02",global.char_bought[2])
ini_write_real("save01","CharBought03",global.char_bought[3])
ini_write_real("save01","CharBought04",global.char_bought[4])
ini_write_real("save01","CharBought05",global.char_bought[5])
ini_write_real("save01","Char",char)
ini_close();
}

