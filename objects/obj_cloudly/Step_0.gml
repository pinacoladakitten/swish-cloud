/// @description Direction & control
if (char = 1 or char = 2 or char = 6)
{
    if dir = 1
    {
    hsp = min(hsp+((maxhsp/10)*(maxhsp/10)),maxhsp)
    }
    if dir = -1
    {
    hsp = max(hsp-((maxhsp/10)*(maxhsp/10)),-1*maxhsp)
    }
}
else
{
    if !mouse_check_button(mb_left)
    if game_start = 1
    {
        if dir = 1
        {
        hsp = min(hsp+((maxhsp/10)*(maxhsp/10)),maxhsp)
        }
        if dir = -1
        {
        hsp = max(hsp-((maxhsp/10)*(maxhsp/10)),-1*maxhsp)
        }
    }
}

if !mouse_check_button(mb_left)
if game_start = 1
{
x+=hsp
}
if mouse_check_button(mb_left)
if game_start = 1
{
    if char = 3 or char = 4 or char = 5
    {
    if !place_meeting(x+1,y,obj_barrier)
        {
        if hsp > 0
            {
            hsp = max(hsp-((maxhsp/15)*(maxhsp/15)),0)
            }
        if hsp < 0
            {
            hsp = min(hsp+((maxhsp/15)*(maxhsp/15)),0)
            }
        }
    if !(hsp = 0)
    if !place_meeting(x+1,y,obj_barrier)
        {
        x+=hsp
        }
        else
        {
        x+=0
        }
    }
    if char = 1 or char = 2 or char = 6
    {
    x+=0
    }
}

if game_start = 1
{
y = max(y-10,992)
}
if dead = 1
{
y += vsp
}

///Meeting Turn Blocks
if place_meeting(x+1,y,obj_turnL)
if tilt_controls = 0
{
    if dir = 1
    if !mouse_check_button(mb_left)
    {
    audio_play_sound(snd_swish,10,false)
    }
dir = -1
}

if place_meeting(x+1,y,obj_turnR)
if tilt_controls = 0
{
    if dir = -1
    if !mouse_check_button(mb_left)
    {
    audio_play_sound(snd_swish,10,false)
    }
dir = 1
}

///Image Angle
if dead = 0
{
image_angle = -hsp
}
if dead = 0
if invi = 1
{
image_blend = c_blue
    with obj_fish_ring
    {
    image_blend = c_blue
    }
}
else
{
image_blend = c_white
    with obj_fish_ring
    {
    image_blend = c_white
    }
}

instance_create(x,y,obj_stream)

///Controlling Direction
if game_start = 1
{
if mouse_check_button(mb_left)
if tilt_controls = 0
{
    if device_mouse_x(0) > obj_cloudly.x
    {
    dir = 1
    }
    if device_mouse_x(0) < obj_cloudly.x
    {
    dir = -1
    }
}
if tilt_controls = 1
    {
    dir = sign(device_get_tilt_z());
    }
}

///Dead
if dead = 1
{
maxhsp = 10
vspe = 5
image_angle += 10
if (vsp < 15) {vsp += grav}
game_start = 0
game_start02 = 0
if !instance_exists(obj_tutorial_text01)
{
with obj_score_card
    {
    spawn = 1
    }
}

if (y > 1376)
    {
    y = 1377
    dead = 0
    with obj_turnL {y = 1376}
    with obj_turnR {y = 1376}
    }
}

///Misc.
if game_start = 1
{

    if mouse_check_button_pressed(mb_left)
    if !instance_exists(obj_tutorial_text01)
    {
    audio_play_sound(snd_stop,10,false)
    }
}

if game_start02 = 0
if game_start = 1
if !instance_exists(obj_start_circle)
if !instance_exists(obj_tutorial_text01)
if mouse_check_button_pressed(mb_left)
{
instance_create(x,y,obj_start_circle)
}

///Meeting Barrier
if place_meeting(x+1,y,obj_barrier)
{
hsp = 0
}

///Characters

//Cloudly
if char = 1
{
sprite_index = spr_cloudly
with obj_stream
    {
    sprite_index = spr_cloudly
    }
}

//Blazler
if char = 2
{
sprite_index = spr_blazler
image_xscale = -dir
with obj_stream
    {
    sprite_index = spr_blazler
    }
}

//Gloomely
if char = 3
{
sprite_index = spr_gloomely
maxhp = 2
with obj_stream
    {
    sprite_index = spr_gloomely
    }
    if hp = 1
    {
    image_blend = c_red
    }
    else
    {
    image_blend = c_white
    }
}
else
{
maxhp = 1
}

//Polagonal
if char = 4
{
sprite_index = spr_polagonal
image_xscale = -dir
if !instance_exists(obj_poly_ring){instance_create(x,y,obj_poly_ring)}
with obj_stream
    {
    sprite_index = spr_polagonal
    }
}
//Fish
if char = 5
{
sprite_index = spr_fish
if !instance_exists(obj_fish_ring){instance_create(x,y,obj_fish_ring)}
with obj_stream
    {
    sprite_index = spr_fish
    }
}
//Sol
if char = 6
{
sprite_index = spr_sun
if !instance_exists(obj_sun_ring){instance_create(x,y,obj_sun_ring)}
with obj_stream
    {
    sprite_index = spr_sun
    }
if image_index < 1
    {
    part_particles_create(global.particles, x, y, global.pt_sun, 2);
    }
}

