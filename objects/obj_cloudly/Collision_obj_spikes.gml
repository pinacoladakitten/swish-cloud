if dead = 0
if invi = 0
{
hp -= 1
    if hp = 0
    {
    instance_create(x,y,obj_hit)
    vsp = -20
    audio_play_sound(snd_hit,10,false)
    dead = 1
    points_total += points
    }
    else
    {
    audio_play_sound(snd_spike_destroy,10,false)
    instance_create(x,y,obj_hit)
    invi = 1
    image_alpha = 0.5
    alarm[1] = 1*room_speed
    }
}

