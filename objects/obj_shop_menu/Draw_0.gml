/// @description Main event
draw_set_color(c_white)
draw_set_alpha(0.5)
draw_rectangle_colour(0,0,room_width,room_height,c_black,c_black,c_black,c_black,0)
draw_set_alpha(1)
draw_sprite(sprite_index,-1,0,0)
draw_set_color(-1)
draw_set_font(font02)
draw_set_halign(fa_center)
draw_text(room_width/2+30,220,string_hash_to_newline("shop"))
draw_set_font(font01)
draw_set_halign(fa_left)
if page = 0{draw_text(15,290,string_hash_to_newline("characters:"))}
if page = 1{draw_text(15,290,string_hash_to_newline("miscellaneous:"))}
draw_set_color(make_color_rgb(255, 255, 140))
draw_set_font(font04)
draw_set_halign(fa_center)
draw_set_font(font01)
draw_text(room_width/2,50,string_hash_to_newline("point bank:"))
draw_set_font(font04)
draw_text(room_width/2,100,string_hash_to_newline(points_total))

///Character Descriptions
draw_set_font(font03)
draw_set_color(c_white)
draw_set_halign(fa_left)
if page = 0
{
if char_select = 1
{
draw_text(15,800,string_hash_to_newline(@"
cloudly:the most basic 
you could get, he has 
no special attributes. 
he focuses on being 
balanced instead of 
relying on shenanigans."))
}
if char_select = 2
{
draw_text(15,800,string_hash_to_newline(@"
blazler:embraces speed
like there's no 
tomorrow!literally 
became a fire ball
from going so fast!
not even my hedgehog
can run this fast!"))
}
if char_select = 3
{
draw_text(15,800,string_hash_to_newline(@"
gloomely:basically the
tank of the group.able 
to take an extra hit 
before giving up. 

however, it comes at a
cost. gloomely is
larger than most,and 
can't exactly stop on a 
dime."))
}
if char_select = 4
{
draw_text(15,800,string_hash_to_newline(@"
polagon:occasionally
shoots a projectile
that pierces through
spikes!

unfortunately polagon 
can't stop on a dime."))
}
if char_select = 5
{
draw_text(15,800,string_hash_to_newline(@"
daoscis:occasinally
goes into a state
that ignores spikes
for a short time.

can't stop on a dime
to compensate."))
}
if char_select = 6
{
draw_text(15,800,string_hash_to_newline(@"
sol:has a fireball that
destroys anything it 
touches.the fireball 
can't be destroyed 
and orbits sol for an
unlimited period of 
time.

sol is,however,the 
largest character."))
}
}
if page = 1
{
if obj_lightswitch_shop.light_select = 1
{
draw_text(15,800,string_hash_to_newline(@"
light switch:use it to
change the time of day
at will.basically,it
makes you a time lord."))
}
}

