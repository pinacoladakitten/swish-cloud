i = 0
if game_start = 1
{
opening = irandom_range(0,4)
if opening = 0 {instance_create(0,y,obj_spike_opening)}
if opening = 1 {instance_create(150,y,obj_spike_opening)}
if opening = 2 {instance_create(300,y,obj_spike_opening)}
if opening = 3 {instance_create(450,y,obj_spike_opening)}
if opening = 4 {instance_create(600,y,obj_spike_opening)}

for(i = 0; i < 5; i += 1)
{
x =(i*150)+25
instance_create(x,y,obj_spikes)
}

vspe = min(vspe+1,vspemax)
spe = max(spe-(spe*0.1),spemax)

alarm[0] = spe*room_speed
}

