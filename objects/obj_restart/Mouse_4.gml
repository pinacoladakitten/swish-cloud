if select = 0
{
audio_play_sound(snd_menu_select,10,false)
alarm[0] = 0.8*room_speed

if instance_exists(obj_spikes)
{
with obj_spikes
    {
    get_out = 1
    }
}
if instance_exists(obj_score_card)
{
with obj_score_card
    {
    get_out = 1
    }
}
with obj_menu
    {
    select = 1
    }
selected = 1
select = 1
}

