image_index += 1
audio_play_sound(snd_text,10,false)
if (image_index = 0) or (image_index > 2)
{
__background_set( e__BG.Index, 0, bg_sky )
__background_set( e__BG.Index, 1, bg_clouds02 )
__background_set( e__BG.Index, 2, bg_clouds02 )
__background_set( e__BG.Index, 3, bg_clouds01 )
__background_set( e__BG.Index, 4, bg_shade )
__background_set( e__BG.XScale, 0, 1 )
__background_set( e__BG.YScale, 0, 1 )
__background_set( e__BG.XScale, 4, 1 )
__background_set( e__BG.YScale, 4, 1 )
}
if image_index = 1
{
__background_set( e__BG.Index, 0, bg_sky_sunset )
__background_set( e__BG.Index, 1, bg_cloudsun02 )
__background_set( e__BG.Index, 2, bg_cloudsun02 )
__background_set( e__BG.Index, 3, bg_cloudsun01 )
__background_set( e__BG.Index, 4, bg_shade_sun )
__background_set( e__BG.XScale, 0, 8 )
__background_set( e__BG.YScale, 0, 8 )
__background_set( e__BG.XScale, 4, 8 )
__background_set( e__BG.YScale, 4, 8 )
}
if image_index = 2
{
__background_set( e__BG.Index, 0, bg_sky_night )
__background_set( e__BG.Index, 1, bg_cloudsnight02 )
__background_set( e__BG.Index, 2, bg_cloudsnight02 )
__background_set( e__BG.Index, 3, bg_cloudsnight01 )
__background_set( e__BG.Index, 4, bg_shade_night )
__background_set( e__BG.XScale, 0, 1 )
__background_set( e__BG.YScale, 0, 1 )
__background_set( e__BG.XScale, 4, 1 )
__background_set( e__BG.YScale, 4, 1 )
}

