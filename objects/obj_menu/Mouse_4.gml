if select = 0
{
instance_create(320,-128,obj_title)
instance_create(-128,608,obj_start)
instance_create(-384,864,obj_tutorial)
instance_create(-160,864,obj_shop)

game_start02 = 0
audio_play_sound(snd_menu_select,10,false)
alarm[0] = 0.8*room_speed

if instance_exists(obj_spikes)
{
with obj_spikes
    {
    get_out = 1
    }
}
if instance_exists(obj_score_card)
{
with obj_score_card
    {
    get_out = 1
    }
}
with obj_restart
    {
    select = 1
    }
selected = 1
select = 1
}

