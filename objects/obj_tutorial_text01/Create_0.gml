pos = 1
text = 0

with obj_cloudly
{
x = 320
}

///Strings
globalvar maxstrings;
maxstrings = 0

for (i = 0; i < maxstrings; i += 1)
{
    global.strings[i] = -1;
}

///Dialogue

strings[0] = 
@"
-you are always
moving side to 
side.

-hold the screen
to stop moving.

-while held,you 
can change your
direction.
-try moving your
finger around.

-then release to
move in that
direction.

you can practice 
here...
"

x = 0
y = 0
image_speed = 0.05

