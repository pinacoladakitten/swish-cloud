/// @description Main event
draw_set_alpha(0.5)
draw_rectangle_colour(0,0,room_width,room_height,c_black,c_black,c_black,c_black,0)
draw_set_alpha(1)
draw_sprite(sprite_index,-1,0,0)

//Setup
draw_set_font(font01);
draw_set_halign(fa_left);
draw_set_color(c_white);
draw_set_alpha(1)

//The Text
cstr = string_copy(strings[text],1,pos)
draw_text_ext_transformed(20,100,string_hash_to_newline(cstr),-1,-1,1,1,0)
pos = min(pos+1.5,255)

