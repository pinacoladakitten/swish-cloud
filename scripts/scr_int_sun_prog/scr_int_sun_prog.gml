function scr_int_sun_prog() {
	//Initialize Particle
	global.pt_sun = part_type_create();
	//Settings
	part_type_sprite(global.pt_sun, spr_sun_part, true, false, true);
	part_type_size(global.pt_sun, 1, 1.2, -0.02, 0);
	part_type_speed(global.pt_sun, 0.4, 0.4, 0, 0);
	part_type_direction(global.pt_sun,0,360,0,0)
	part_type_life(global.pt_sun, 50, 50);



}
