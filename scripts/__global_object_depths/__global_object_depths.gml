function __global_object_depths() {
	// Initialise the global array that allows the lookup of the depth of a given object
	// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
	// NOTE: MacroExpansion is used to insert the array initialisation at import time
	gml_pragma( "global", "__global_object_depths()");

	// insert the generated arrays here
	global.__objectDepths[0] = -2; // obj_cloudly
	global.__objectDepths[1] = 0; // obj_poly_proj
	global.__objectDepths[2] = 0; // obj_poly_ring
	global.__objectDepths[3] = 0; // obj_fish_ring
	global.__objectDepths[4] = -10; // obj_sun_ring
	global.__objectDepths[5] = 0; // obj_bg_control
	global.__objectDepths[6] = -10000000; // obj_globalvars
	global.__objectDepths[7] = -10; // particle_sys
	global.__objectDepths[8] = -10000; // obj_start
	global.__objectDepths[9] = -20000000; // obj_restart
	global.__objectDepths[10] = -20000; // obj_menu
	global.__objectDepths[11] = 1; // obj_tutorial
	global.__objectDepths[12] = 1; // obj_shop
	global.__objectDepths[13] = -100; // obj_title
	global.__objectDepths[14] = 0; // obj_char_select01
	global.__objectDepths[15] = 0; // obj_char_select02
	global.__objectDepths[16] = 0; // obj_char_select03
	global.__objectDepths[17] = 0; // obj_char_select04
	global.__objectDepths[18] = 0; // obj_char_select05
	global.__objectDepths[19] = 0; // obj_char_select06
	global.__objectDepths[20] = -100; // obj_get
	global.__objectDepths[21] = 0; // obj_lightswitch_shop
	global.__objectDepths[22] = 100; // obj_shop_menu
	global.__objectDepths[23] = 0; // obj_shop_next
	global.__objectDepths[24] = 0; // obj_turnR
	global.__objectDepths[25] = 0; // obj_turnL
	global.__objectDepths[26] = 10; // obj_spikes
	global.__objectDepths[27] = 0; // obj_obs_spawner
	global.__objectDepths[28] = 0; // obj_spike_destroy
	global.__objectDepths[29] = 0; // obj_spike_opening
	global.__objectDepths[30] = -1000000; // obj_spike_shop_default
	global.__objectDepths[31] = 0; // obj_tutorial_text01
	global.__objectDepths[32] = -1; // obj_stream
	global.__objectDepths[33] = 0; // obj_point
	global.__objectDepths[34] = -5; // obj_hit
	global.__objectDepths[35] = -1000000; // obj_score_card
	global.__objectDepths[36] = 0; // obj_back_out
	global.__objectDepths[37] = 0; // obj_barrier
	global.__objectDepths[38] = 0; // obj_start_circle


	global.__objectNames[0] = "obj_cloudly";
	global.__objectNames[1] = "obj_poly_proj";
	global.__objectNames[2] = "obj_poly_ring";
	global.__objectNames[3] = "obj_fish_ring";
	global.__objectNames[4] = "obj_sun_ring";
	global.__objectNames[5] = "obj_bg_control";
	global.__objectNames[6] = "obj_globalvars";
	global.__objectNames[7] = "particle_sys";
	global.__objectNames[8] = "obj_start";
	global.__objectNames[9] = "obj_restart";
	global.__objectNames[10] = "obj_menu";
	global.__objectNames[11] = "obj_tutorial";
	global.__objectNames[12] = "obj_shop";
	global.__objectNames[13] = "obj_title";
	global.__objectNames[14] = "obj_char_select01";
	global.__objectNames[15] = "obj_char_select02";
	global.__objectNames[16] = "obj_char_select03";
	global.__objectNames[17] = "obj_char_select04";
	global.__objectNames[18] = "obj_char_select05";
	global.__objectNames[19] = "obj_char_select06";
	global.__objectNames[20] = "obj_get";
	global.__objectNames[21] = "obj_lightswitch_shop";
	global.__objectNames[22] = "obj_shop_menu";
	global.__objectNames[23] = "obj_shop_next";
	global.__objectNames[24] = "obj_turnR";
	global.__objectNames[25] = "obj_turnL";
	global.__objectNames[26] = "obj_spikes";
	global.__objectNames[27] = "obj_obs_spawner";
	global.__objectNames[28] = "obj_spike_destroy";
	global.__objectNames[29] = "obj_spike_opening";
	global.__objectNames[30] = "obj_spike_shop_default";
	global.__objectNames[31] = "obj_tutorial_text01";
	global.__objectNames[32] = "obj_stream";
	global.__objectNames[33] = "obj_point";
	global.__objectNames[34] = "obj_hit";
	global.__objectNames[35] = "obj_score_card";
	global.__objectNames[36] = "obj_back_out";
	global.__objectNames[37] = "obj_barrier";
	global.__objectNames[38] = "obj_start_circle";


	// create another array that has the correct entries
	var len = array_length_1d(global.__objectDepths);
	global.__objectID2Depth = [];
	for( var i=0; i<len; ++i ) {
		var objID = asset_get_index( global.__objectNames[i] );
		if (objID >= 0) {
			global.__objectID2Depth[ objID ] = global.__objectDepths[i];
		} // end if
	} // end for


}
