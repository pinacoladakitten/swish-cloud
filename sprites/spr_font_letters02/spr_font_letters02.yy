{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 71,
  "bbox_top": 0,
  "bbox_bottom": 71,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 72,
  "height": 72,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"aa6dc4db-9540-46c9-ad6f-d8e34d27ff96","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa6dc4db-9540-46c9-ad6f-d8e34d27ff96","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"aa6dc4db-9540-46c9-ad6f-d8e34d27ff96","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aebb5e66-adb5-427b-90cf-9aa7474386b5","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aebb5e66-adb5-427b-90cf-9aa7474386b5","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"aebb5e66-adb5-427b-90cf-9aa7474386b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc72f696-f71d-4529-ab4f-7d7c53d95fcb","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc72f696-f71d-4529-ab4f-7d7c53d95fcb","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"fc72f696-f71d-4529-ab4f-7d7c53d95fcb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"62c4ddbe-749a-4f5e-af18-a7f219723aee","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"62c4ddbe-749a-4f5e-af18-a7f219723aee","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"62c4ddbe-749a-4f5e-af18-a7f219723aee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1418343e-d0ca-4099-91ac-d8c70c316f1e","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1418343e-d0ca-4099-91ac-d8c70c316f1e","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"1418343e-d0ca-4099-91ac-d8c70c316f1e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"95654f2f-0501-4a01-b5f5-9e61cc1095a6","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"95654f2f-0501-4a01-b5f5-9e61cc1095a6","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"95654f2f-0501-4a01-b5f5-9e61cc1095a6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"39f88814-3774-407f-80c8-61ae69ece8d1","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"39f88814-3774-407f-80c8-61ae69ece8d1","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"39f88814-3774-407f-80c8-61ae69ece8d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3e0a0d89-0879-4200-8d5a-6efb7364d2d6","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3e0a0d89-0879-4200-8d5a-6efb7364d2d6","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"3e0a0d89-0879-4200-8d5a-6efb7364d2d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6a0e6ab1-e561-402c-9c48-139e13a0c830","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6a0e6ab1-e561-402c-9c48-139e13a0c830","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"6a0e6ab1-e561-402c-9c48-139e13a0c830","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"024f22c5-8919-4db8-a153-b25f86fb0f29","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"024f22c5-8919-4db8-a153-b25f86fb0f29","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"024f22c5-8919-4db8-a153-b25f86fb0f29","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bd4b3a2a-3419-4b66-9c18-2927cefffd85","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bd4b3a2a-3419-4b66-9c18-2927cefffd85","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"bd4b3a2a-3419-4b66-9c18-2927cefffd85","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"591e8432-f4a5-4610-90a3-b15013c50f58","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"591e8432-f4a5-4610-90a3-b15013c50f58","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"591e8432-f4a5-4610-90a3-b15013c50f58","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"50ac4961-7a4d-4ce3-b196-60632d45f326","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"50ac4961-7a4d-4ce3-b196-60632d45f326","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"50ac4961-7a4d-4ce3-b196-60632d45f326","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a273b13c-4a9f-4173-ad5e-c3a9b5e0da85","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a273b13c-4a9f-4173-ad5e-c3a9b5e0da85","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"a273b13c-4a9f-4173-ad5e-c3a9b5e0da85","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d3a51af-ef5e-4955-a0aa-06875061cab0","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d3a51af-ef5e-4955-a0aa-06875061cab0","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"1d3a51af-ef5e-4955-a0aa-06875061cab0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fdd7fab2-d91c-4ec6-8953-836d7a2de12e","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fdd7fab2-d91c-4ec6-8953-836d7a2de12e","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"fdd7fab2-d91c-4ec6-8953-836d7a2de12e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"050394c7-03de-4038-aaf2-5f0c02af7681","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"050394c7-03de-4038-aaf2-5f0c02af7681","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"050394c7-03de-4038-aaf2-5f0c02af7681","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6e445fa0-fdf3-4a0e-8bf4-b0dc9efaed87","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6e445fa0-fdf3-4a0e-8bf4-b0dc9efaed87","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"6e445fa0-fdf3-4a0e-8bf4-b0dc9efaed87","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c0bb186-d5a9-43f2-abcc-ca2b9452af63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c0bb186-d5a9-43f2-abcc-ca2b9452af63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"3c0bb186-d5a9-43f2-abcc-ca2b9452af63","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6bf64ed7-eccc-4c23-b972-5bc7cc5d4777","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6bf64ed7-eccc-4c23-b972-5bc7cc5d4777","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"6bf64ed7-eccc-4c23-b972-5bc7cc5d4777","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"92221e23-0580-47a6-aa53-74cfe938a151","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"92221e23-0580-47a6-aa53-74cfe938a151","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"92221e23-0580-47a6-aa53-74cfe938a151","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4b522ebd-69cd-4aa0-a41a-e8c0a3ffb571","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4b522ebd-69cd-4aa0-a41a-e8c0a3ffb571","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"4b522ebd-69cd-4aa0-a41a-e8c0a3ffb571","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9b8bf191-f0e8-4241-b775-7a6b20592e06","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9b8bf191-f0e8-4241-b775-7a6b20592e06","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"9b8bf191-f0e8-4241-b775-7a6b20592e06","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fe9c5a85-2cb2-4a84-b3dc-f86c5824a16c","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fe9c5a85-2cb2-4a84-b3dc-f86c5824a16c","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"fe9c5a85-2cb2-4a84-b3dc-f86c5824a16c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e0c70b40-ca28-4b56-a675-52e0f06cba53","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e0c70b40-ca28-4b56-a675-52e0f06cba53","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"e0c70b40-ca28-4b56-a675-52e0f06cba53","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"96b6edb2-90c3-437f-b116-bc45e67dfd64","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96b6edb2-90c3-437f-b116-bc45e67dfd64","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"96b6edb2-90c3-437f-b116-bc45e67dfd64","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"08f4d591-8d4b-4dcd-b522-2cf901ab809d","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"08f4d591-8d4b-4dcd-b522-2cf901ab809d","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"08f4d591-8d4b-4dcd-b522-2cf901ab809d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"abb20c9a-4162-4704-a041-c09a8e3a3fa2","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"abb20c9a-4162-4704-a041-c09a8e3a3fa2","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"abb20c9a-4162-4704-a041-c09a8e3a3fa2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a112733a-f257-49d6-bc33-34b1e8c97a75","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a112733a-f257-49d6-bc33-34b1e8c97a75","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"a112733a-f257-49d6-bc33-34b1e8c97a75","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2234ddea-b792-4571-a730-f10a89e1c1d7","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2234ddea-b792-4571-a730-f10a89e1c1d7","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"2234ddea-b792-4571-a730-f10a89e1c1d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"efafd927-80da-4412-a169-d1e077b12723","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"efafd927-80da-4412-a169-d1e077b12723","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"LayerId":{"name":"838faa8b-5ad3-448b-839e-6c52078a7d63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","name":"efafd927-80da-4412-a169-d1e077b12723","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 31.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6b9eba0e-7bd4-4dee-a9eb-9e4df2c82175","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa6dc4db-9540-46c9-ad6f-d8e34d27ff96","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d29988a5-d861-49eb-8a0a-92cfa94feeb6","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aebb5e66-adb5-427b-90cf-9aa7474386b5","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8427670-597c-49e3-8394-891eaada48c8","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc72f696-f71d-4529-ab4f-7d7c53d95fcb","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"310640ff-1bb1-412c-8095-ec969c7b807a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62c4ddbe-749a-4f5e-af18-a7f219723aee","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"849bd382-aecd-4c0e-ac57-2682dc5019f9","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1418343e-d0ca-4099-91ac-d8c70c316f1e","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"658cf4f6-7b5e-4de5-91b8-9f00f27d9b35","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"95654f2f-0501-4a01-b5f5-9e61cc1095a6","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2715b818-24e1-4ef5-8b54-6f4bbaa7de4c","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"39f88814-3774-407f-80c8-61ae69ece8d1","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"adf3689c-f43a-47eb-9917-da29180b5753","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3e0a0d89-0879-4200-8d5a-6efb7364d2d6","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4f77aa4a-aa36-4fe6-a34e-c656c0acab43","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6a0e6ab1-e561-402c-9c48-139e13a0c830","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8163edeb-2ceb-4cd8-9332-692072cb00b3","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"024f22c5-8919-4db8-a153-b25f86fb0f29","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2dd0bc06-c9c3-42e4-a49d-5218528a6a1e","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bd4b3a2a-3419-4b66-9c18-2927cefffd85","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d053e8fa-1990-4dc3-b23d-7e390485757d","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"591e8432-f4a5-4610-90a3-b15013c50f58","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0aee535c-f1bb-4429-b3d6-9fc954bc1992","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"50ac4961-7a4d-4ce3-b196-60632d45f326","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6fe825ec-ff01-4f1c-8171-54f80bf721b5","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a273b13c-4a9f-4173-ad5e-c3a9b5e0da85","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8b620edd-4e1c-49c9-ae70-28ec925d7a5b","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d3a51af-ef5e-4955-a0aa-06875061cab0","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a328b26-1cf0-4143-b9b3-c6aa05ffd579","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fdd7fab2-d91c-4ec6-8953-836d7a2de12e","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"492b972d-b1ef-4c7a-9f34-6e139f31d530","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"050394c7-03de-4038-aaf2-5f0c02af7681","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"74993f3c-9b1d-4690-8e58-72cb43aaafd9","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6e445fa0-fdf3-4a0e-8bf4-b0dc9efaed87","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"190209f1-e8ce-4580-bcdd-1faf054480b6","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c0bb186-d5a9-43f2-abcc-ca2b9452af63","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ee53a46-2bb3-4815-8980-659f7590e5a6","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6bf64ed7-eccc-4c23-b972-5bc7cc5d4777","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"265d9cd4-8421-4697-ae83-454147ce89cd","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"92221e23-0580-47a6-aa53-74cfe938a151","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a4528dfd-1ec2-4ea1-92bb-5fe6ff6d0cb5","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4b522ebd-69cd-4aa0-a41a-e8c0a3ffb571","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d55e2e51-4645-4da8-8aea-d4258086abd2","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9b8bf191-f0e8-4241-b775-7a6b20592e06","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7e058443-06af-4233-b0c7-cb6c217492a1","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fe9c5a85-2cb2-4a84-b3dc-f86c5824a16c","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f3a90a42-6368-4fdb-841f-bc1404cfec82","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e0c70b40-ca28-4b56-a675-52e0f06cba53","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2290aa52-2bec-4548-aed1-2b8fbfba5a8e","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96b6edb2-90c3-437f-b116-bc45e67dfd64","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1b39ecb0-abb2-4457-8c50-d0b68784a01f","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"08f4d591-8d4b-4dcd-b522-2cf901ab809d","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"149670b5-6843-4181-94bd-572b9421be67","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"abb20c9a-4162-4704-a041-c09a8e3a3fa2","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f146feb6-599a-4d74-84f6-97d7f80c8b14","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a112733a-f257-49d6-bc33-34b1e8c97a75","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1a0efa9-b584-48e5-ac99-3c229da25ddc","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2234ddea-b792-4571-a730-f10a89e1c1d7","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"40fea704-bb64-41ce-8d57-623aea30b3e9","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"efafd927-80da-4412-a169-d1e077b12723","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 27,
    "yorigin": 39,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_font_letters02","path":"sprites/spr_font_letters02/spr_font_letters02.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"838faa8b-5ad3-448b-839e-6c52078a7d63","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "fonts",
    "path": "folders/Sprites/fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_font_letters02",
  "tags": [],
  "resourceType": "GMSprite",
}