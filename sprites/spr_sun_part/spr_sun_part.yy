{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 47,
  "bbox_top": 0,
  "bbox_bottom": 45,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 48,
  "height": 48,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8e781970-d7cf-43ab-b00a-bb7fb36be9fa","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8e781970-d7cf-43ab-b00a-bb7fb36be9fa","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"8e781970-d7cf-43ab-b00a-bb7fb36be9fa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb775eee-f451-4c61-bc7d-b6e03d3db0df","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb775eee-f451-4c61-bc7d-b6e03d3db0df","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"eb775eee-f451-4c61-bc7d-b6e03d3db0df","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"68a6d751-7ba5-43c7-83e0-635c3d00be3d","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"68a6d751-7ba5-43c7-83e0-635c3d00be3d","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"68a6d751-7ba5-43c7-83e0-635c3d00be3d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"67a6e7e2-a080-425a-b6ce-e72def9a1e93","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"67a6e7e2-a080-425a-b6ce-e72def9a1e93","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"67a6e7e2-a080-425a-b6ce-e72def9a1e93","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b2babd02-0c6a-41ad-8d4f-8331cf0d6247","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b2babd02-0c6a-41ad-8d4f-8331cf0d6247","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"b2babd02-0c6a-41ad-8d4f-8331cf0d6247","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2efdf9a7-1b50-4c05-89f9-7d2c24047f68","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2efdf9a7-1b50-4c05-89f9-7d2c24047f68","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"2efdf9a7-1b50-4c05-89f9-7d2c24047f68","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"315f5917-ba85-4769-a754-27b27250d9a5","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"315f5917-ba85-4769-a754-27b27250d9a5","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"315f5917-ba85-4769-a754-27b27250d9a5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"48920e6e-2482-4426-b969-59e9603930da","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"48920e6e-2482-4426-b969-59e9603930da","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"48920e6e-2482-4426-b969-59e9603930da","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cf97a4f6-a151-4490-a940-73d2f91502fb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cf97a4f6-a151-4490-a940-73d2f91502fb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"cf97a4f6-a151-4490-a940-73d2f91502fb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d7a87637-a4bc-4894-8415-39950890a16b","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d7a87637-a4bc-4894-8415-39950890a16b","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"d7a87637-a4bc-4894-8415-39950890a16b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cd26b1d4-4536-4ac9-8dea-5e90cd7ee990","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cd26b1d4-4536-4ac9-8dea-5e90cd7ee990","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"cd26b1d4-4536-4ac9-8dea-5e90cd7ee990","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fce22a24-d5e9-467e-8976-243a7eaa946b","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fce22a24-d5e9-467e-8976-243a7eaa946b","path":"sprites/spr_sun_part/spr_sun_part.yy",},"LayerId":{"name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","name":"fce22a24-d5e9-467e-8976-243a7eaa946b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 12.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5c6a20f1-fadf-4fb8-bd5f-84fd5b429003","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8e781970-d7cf-43ab-b00a-bb7fb36be9fa","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e7c016e-de08-4265-8705-26044e12a63b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb775eee-f451-4c61-bc7d-b6e03d3db0df","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"753e020d-a74d-46b2-b926-9779f659317b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"68a6d751-7ba5-43c7-83e0-635c3d00be3d","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e79cf760-a500-4476-b0dc-5880a78c5134","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"67a6e7e2-a080-425a-b6ce-e72def9a1e93","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"39f13e83-7eb7-4b99-9aa8-ad925939a3ad","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b2babd02-0c6a-41ad-8d4f-8331cf0d6247","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"70958c4b-be8f-43b4-8fa4-3b22a5627920","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2efdf9a7-1b50-4c05-89f9-7d2c24047f68","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0646218-495a-4e72-9aa5-1193cf19056e","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"315f5917-ba85-4769-a754-27b27250d9a5","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9889c11e-ce46-4573-b2c9-f3f694a6d4c8","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"48920e6e-2482-4426-b969-59e9603930da","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4f88b94b-bc4f-4be9-8671-b1f34534a61f","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cf97a4f6-a151-4490-a940-73d2f91502fb","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3ec610c-5f01-47b6-8579-9923e50160e5","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d7a87637-a4bc-4894-8415-39950890a16b","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"95494de7-56e0-46d0-bde0-1779b43dab4f","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cd26b1d4-4536-4ac9-8dea-5e90cd7ee990","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"54707681-914b-405a-aed1-1da1008e2d19","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fce22a24-d5e9-467e-8976-243a7eaa946b","path":"sprites/spr_sun_part/spr_sun_part.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 24,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_sun_part","path":"sprites/spr_sun_part/spr_sun_part.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"32f631b1-9a93-4816-9abc-88a6cf30abfb","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "sun",
    "path": "folders/Sprites/characters/sun.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_sun_part",
  "tags": [],
  "resourceType": "GMSprite",
}