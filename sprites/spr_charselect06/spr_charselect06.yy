{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 19,
  "bbox_right": 174,
  "bbox_top": 18,
  "bbox_bottom": 173,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 192,
  "height": 192,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"efecebac-43b2-4dec-b60d-64b42fe45282","path":"sprites/spr_charselect06/spr_charselect06.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"efecebac-43b2-4dec-b60d-64b42fe45282","path":"sprites/spr_charselect06/spr_charselect06.yy",},"LayerId":{"name":"d6fa7124-8bfc-4994-bafe-6aef4f2d8c38","path":"sprites/spr_charselect06/spr_charselect06.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_charselect06","path":"sprites/spr_charselect06/spr_charselect06.yy",},"resourceVersion":"1.0","name":"efecebac-43b2-4dec-b60d-64b42fe45282","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"995adee5-4645-43bc-97be-1688f2be356a","path":"sprites/spr_charselect06/spr_charselect06.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"995adee5-4645-43bc-97be-1688f2be356a","path":"sprites/spr_charselect06/spr_charselect06.yy",},"LayerId":{"name":"d6fa7124-8bfc-4994-bafe-6aef4f2d8c38","path":"sprites/spr_charselect06/spr_charselect06.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_charselect06","path":"sprites/spr_charselect06/spr_charselect06.yy",},"resourceVersion":"1.0","name":"995adee5-4645-43bc-97be-1688f2be356a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_charselect06","path":"sprites/spr_charselect06/spr_charselect06.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7d54e79e-f5fd-4125-9a41-944834a480e0","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"efecebac-43b2-4dec-b60d-64b42fe45282","path":"sprites/spr_charselect06/spr_charselect06.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bf304887-3b5d-4ab7-8610-7c6cfbd41c78","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"995adee5-4645-43bc-97be-1688f2be356a","path":"sprites/spr_charselect06/spr_charselect06.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 96,
    "yorigin": 96,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_charselect06","path":"sprites/spr_charselect06/spr_charselect06.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d6fa7124-8bfc-4994-bafe-6aef4f2d8c38","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "menu",
    "path": "folders/Sprites/menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_charselect06",
  "tags": [],
  "resourceType": "GMSprite",
}